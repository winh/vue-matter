import { World } from 'matter-js'

export const isPhysicsComponent = component =>
  component && component.$options.body

export const addToWorld = component => {
  const { body } = component.$options
  const { world } = component.engine
  World.add(world, body)
}

export const removeFromWorld = component => {
  const { body } = component.$options
  const { world } = component.engine
  World.remove(world, body)
}

export const synchronizeProperty = (sourceObject, targetObject, key) => {
  Object.defineProperty(targetObject, key, {
    get() {
      return sourceObject[key]
    },
    set(newValue) {
      if (newValue === sourceObject[key]) {
        return
      }

      sourceObject[key] = newValue
    }
  })
}

export const collisionEvents = [
  'collisionActive',
  'collisionEnd',
  'collisionStart'
]

const filterOptions = (component, optionKeys) => {
  const options = {}
  optionKeys.forEach(key => {
    if (component[key]) {
      options[key] = component[key]
    }
  })
  return options
}

const bodyOptionKeys = ['isStatic', 'label']
export const filterBodyOptions = component =>
  filterOptions(component, bodyOptionKeys)

const constraintOptionKeys = [
  'damping',
  'label',
  'length',
  'pointA',
  'pointB',
  'stiffness'
]
export const filterConstraintOptions = component =>
  filterOptions(component, constraintOptionKeys)
