import { Bodies } from 'matter-js'
import bodyMixin from './body'
import { filterBodyOptions } from '../helpers'

export default bodyMixin(component => {
  const { position } = component
  const { x, y } = position
  return Bodies.rectangle(
    x,
    y,
    component.width,
    component.height,
    filterBodyOptions(component)
  )
})
