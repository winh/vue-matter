import { Body, Events } from 'matter-js'
import {
  addToWorld,
  collisionEvents,
  removeFromWorld,
  synchronizeProperty
} from '../helpers'

export default factory => ({
  inject: ['engine'],

  props: {
    angle: {
      type: Number,
      required: false,
      default: 0
    },
    label: {
      type: String,
      required: false,
      default: null
    },
    position: {
      type: Object,
      required: false,
      default: () => ({ x: 0, y: 0 })
    },
    isStatic: {
      type: Boolean,
      required: false,
      default: false
    }
  },

  data() {
    const physics = {
      angle: this.angle,
      position: { ...this.position }
    }

    return {
      physics
    }
  },

  watch: {
    isStatic(newValue) {
      const { body } = this.$options
      Body.setStatic(body, newValue)
    },

    position(newPosition) {
      const { body } = this.$options
      Body.setPosition(body, newPosition)
    }
  },

  created() {
    const body = factory(this)
    synchronizeProperty(this.physics, body, 'angle')
    synchronizeProperty(this, body, 'label')
    body.position = this.physics.position
    Object.assign(this.$options, { body })

    addToWorld(this)

    collisionEvents.forEach(eventName => {
      const listener = this.$listeners[eventName]
      if (listener) {
        Events.on(this.engine, eventName, this.handleCollisionEvent)
      }
    })
  },

  destroyed() {
    removeFromWorld(this)
  },

  methods: {
    handleCollisionEvent(event) {
      const { body } = this.$options
      const { name, pairs } = event
      const isAffected = pairs.some(pair => {
        const { bodyA, bodyB } = pair
        return bodyA === body || bodyB === body
      })

      if (isAffected) {
        this.$emit(name, event)
      }
    }
  }
})
