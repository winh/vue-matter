import { Bodies } from 'matter-js'
import bodyMixin from './body'
import { filterBodyOptions } from '../helpers'

export default bodyMixin(component => {
  const { x, y } = component.position
  console.log('x, y, component.radius', x, y, component.radius)
  return Bodies.circle(x, y, component.radius, filterBodyOptions(component))
})
