import { Constraint } from 'matter-js'
import {
  addToWorld,
  removeFromWorld,
  filterConstraintOptions,
  synchronizeProperty,
  isPhysicsComponent
} from '../helpers'

export default {
  inject: ['engine'],

  props: {
    damping: {
      type: Number,
      required: false,
      default: 0
    },
    label: {
      type: String,
      required: false,
      default: null
    },
    length: {
      type: Number,
      required: false,
      default: 0
    },
    pointA: {
      type: Object,
      required: false,
      default: () => null
    },
    pointB: {
      type: Object,
      required: false,
      default: () => null
    },
    stiffness: {
      type: Number,
      required: false,
      default: 1
    }
  },

  mounted() {
    const childBodies = this.$children
      .filter(isPhysicsComponent)
      .map(component => component.$options.body)

    if (childBodies.length > 2) {
      throw new Error('Constraint can have at most two bodies attached!')
    }

    const [bodyA, bodyB] = childBodies

    const params = {
      ...filterConstraintOptions(this),
      bodyA,
      bodyB
    }
    const body = Constraint.create(params)
    Object.assign(this.$options, { body })

    addToWorld(this)
  },

  destroyed() {
    removeFromWorld(this)
  }
}
